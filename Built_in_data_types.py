'''
Three categories: numeric, sequence and mapping.
To represent Null value, we have None. None type is Immutable.

1] Numeric:

int --- Integer
float --- float
complex --- complex number
( a = 2 + 3j where a.real and a.imag)
bool --- Boolean

2] Sequences:

str --- String
list --- list of arbitary objects
Tuple --- Group of arbitary items
range --- creates a range of integers

3] Mapping:

dict --- dictionary of k-v pairs
set --- mutable, unordered collection of unique items
frosenset --- Immutable set
'''

import decimal
x = decimal.Decimal(3.14); y = decimal.Decimal(2.74)
x * y
decimal.getcontext().prec = 4

import fractions
fractions.Fraction(3,4) # creates the fraction 3/4



#About Sequences

len(s)
min(s, [,default = obj, key = func]) #Minimum value in s (alphabetically for strings)
max(s, [,default = obj, key = func]) #Maximum value in s (alphabetically for strings)
sum(s, [,start=0]) #The sum of elements(returns TypeError if s is not numeric)
all(s)  #returns True if all elements in s are True( that is, not 0, False, or NULL)
any(s)  #Checks whether any item in s is True.

s + r #concatenates two sequences of the same type
s * n #Make n copies of s, where n is an integer
v1, v2 ..., vn = s #Unpacks n variables from s to v1, v2 and so on
s[i] #Indexing-returns element i of s
s[i:j:stride] #Slicing returns elements between i and j with optional stride
x in s #Returns True if element x is in s
x not in s #Returns true if element x is not in s


#Tuples
'''
Tuples are hashable
'''

tpl = ('a', 'b', 'c') #Without trailing commas, this would be intepreted as a string.
tuple('sequence')

l = ['one', 'two']
x,y = l #assigns x and y to 'one' and 'two' respectively.

#Swapping valules using multiple assignments
x,y = y,x #x = 'two' and y = 'one'


#Dictionaries
len(d) #Number of items in d
d.clear() #Removes all items from d
d.copy() #Makes a shallow copy of d
d.fromkeys(s, [,value]) #Returns a new dictionary with keys from sequence s and values set to value.


d = {'one': 1, 'two': 2, 'three': 3} #creates a dictionary
d['four'] = 4 # add an item
d.update({'five': 5, 'six': 6}) # add multiple items
'five' in d #returns True


#Sorting Dictionaries
sorted(list(d)) #sorts keys
sorted(list(d.values())) #sort values

#sorted method has two optional arguments: key and reverse
#using getitem special method to sort dictionary keys according to dictionary values.
sorted(list(d), key = d.__getitem__)

[value for (key, value) in sorted(d.items())]

sorted(list(d), key = d.__getitem__, reverse = True)

d2 = {'one':'uno' , 'two':'deux', 'three':'trois', 'four': 'quatre', 'five': 'cinq', 'six':'six'}

sorted(d2, key = d.__getitem__)

[d2[i] for i in sorted(d2, key = d.__getitem__)]


#Dictionaries for text analysis
def wordcount(fname):
    try:
        fhand = open(fname)
    except:
        print('File cannot be opened')
        exit()

    count = dict()

    for line in fhand:
        words = line.split()
        for word in words:
            if word is not in count:
                count[word] = 1
            else:
                count[word] += 1
    return(count)
    
