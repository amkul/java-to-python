def multiply_by_2(data):
    return data*2

def do_this_and_print(func, data):
    print(func(data))

do_this_and_print(multiply_by_2, 5)
