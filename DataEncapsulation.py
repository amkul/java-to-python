'''
To define private methods, we use __ in python. __privateMethod().
These method names are automatically changed to _class-name_privateMethod() to prevent name conflicts with methods defined in base classes.
This does not strictly hide private attributes rather it just provides a mechanism for preventing name conflicts.
'''

'''
It is recommended to use private attributes when using a class property to define mutable attributes.
'''

class Bexp(Aexp):
    __base = 3

    def __exp(self):
        return(x**cls.base)
