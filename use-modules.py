import statistics
marks = [1, 6, 9, 23, 2]
print(statistics.median(marks))

from collections import deque
queue = deque(['Zero', 'One', 'Two'])
queue.pop()
queue.append('Four')
queue.append('Five')
queue.appendleft('Minus One')
print(queue)

import datetime
print(datetime.datetime.today())
