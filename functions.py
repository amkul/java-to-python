'''
functions and classes are called first class objects.
'''

def greeting(language):
    if language == 'eng':
        return 'hello world'

        if language == 'fr':
            return 'Bonjur Le Monde'
        else:
            return 'language not supported'

def callf(f):
    lang = 'lang'
    return (f(lang))

lst = [1,2,3,4]
print(list(map(lambda x: x**3, lst)))
print(list(filter((lambda x: x<3), lst)))

greetlist = [greeting('eng'), greeting('fr'), greeting('ger')]
print(greetlist[0])
print(callf(greeting))
