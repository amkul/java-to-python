# Tip 1 - Getting index of element
numbers = [1, 4, 6, 3, 4]
for number in numbers:
    print(number)

for index, number in enumerate(numbers):
    print(f'{index} : {number}')

values = list('aeiou')
for index, vowel in enumerate(values):
    print(f'{index} : {vowel}')


# Tip 2 - Enum in Python
from enum import Enum

class Currency(Enum):
    USD = 1
    EUR = 2
    INR = 3

for currency in Currency:
    print(currency)

print(Currency(1).name)
print(Currency(1).value)
# print(Currency.USD)
# print(Currency.INR)

# Tip 3 - Methods and arguments
def example_method(mandatory_parameter, default_parameter='Default', *args, **kwargs):
    print(f'''
        mandatory_parameter={mandatory_parameter} {type(mandatory_parameter)}
        default_parameter={default_parameter} {type(default_parameter)}
        args={args} {type(args)}
        kwargs={kwargs} {type(kwargs)}
        ''')

example_method(mandatory_parameter=15)
example_method(25, 45)
example_method(25, 'String 1', 'String 2', 'String 3', 'String 4', 'String 5')
example_method(25, 'String 1', 'String 2', 'String 3', 'String 4', 'String 5', key1='a', key2='b')
example_method(25, 'String 1', 'String 2', 'String 3', 'String 4', 'String 5', key1='a', key2='b')
