# def multiply_by_3(data):
#     return data*3
#
# do_this_and_print(multiply_by_3, 5)

def do_this_and_print(func, data):
    print(func(data))

do_this_and_print(lambda data:data*3, 125)
do_this_and_print(lambda data:data*5, 125)
do_this_and_print(lambda data:data*data, 125)
do_this_and_print(lambda data:data ** 3, 125)
do_this_and_print(lambda data:len(data), 'Test')

numbers = [1, 89, 54, 35]
# odd numbers only
print(list(filter(lambda x:x%2==1, numbers)))
# even numbers only
print(list(filter(lambda x:x%2==0, numbers)))
# any non-zero value is considered to be true
print(list(filter(lambda x:x%2, numbers)))

words = ['Apple', 'Ant', 'Bat']
print(list(filter(lambda word:word.endswith('at'), words)))

# words having length=3
print(list(filter(lambda word:len(word)==3, words)))

# words that start with A
print(list(filter(lambda word:word.startswith('A'), words)))

# mapping words to uppercase
# words = ['APPLE', 'ANT', 'BAT']
print(list(map(lambda word:word.upper(), words)))
print(list(map(lambda word:len(word), words)))

nums = [1, 5, 2, 9]
print(list(map(lambda x:x*x, nums)))

print(list(map(lambda x:x*x, range(1, 11))))

numbers = [3, 15, 12, 10]
sum(numbers)
max(numbers)

from functools import reduce
print(reduce(lambda x,y:x*y, numbers))
print(reduce(lambda x,y:max(x, y), numbers))

words = ['Apple', 'Ant', 'Bat']
print(reduce(lambda x,y: x if len(x) > len(y) else y, words))

# combining map, filter and reduce
numbers = [3, 7, 8, 15, 24, 35, 46]
print(reduce(lambda x,y:x+y, map(lambda x:x*x, filter(lambda x:x%2==0, numbers))))

# Part 2
months = [('Jan', 31), ('Feb', 28), ('Mar', 31)]
tuple_ex = ('Dec', 31)
print(tuple_ex[0])
print(tuple_ex[1])
map(lambda x:x[1], months)
print(sum(map(lambda x:x[1], months)))

print(reduce(lambda x,y: x if x[1] < y[1] else y, months))
print(reduce(lambda x,y: x if x[1] < y[1] else y, months)[0])
