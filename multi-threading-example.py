# Python program to illustrate the concept
# of threading
# importing the threading module

import threading

class MultiThreading:
    def print_cube(self, num):
        """
        print cube of a given number
        """
        print('Cube: {}'.format(num*num*num))

    def print_square(self, num):
        """
        print cube of a given number
        """
        print('Square: {}'.format(num*num))

if __name__ == "__main__":
    mt = MultiThreading()
    t1 = threading.Thread(target=mt.print_square, args=(5,))
    t2 = threading.Thread(target=mt.print_cube, args=(5,))

    # starting thread 1
    t1.start()
    # starting thread 2
    t2.start()
    # wait until thread 1 is completely executed
    t1.join()
    # wait until thread 2 is completely executed
    t2.join()

    print('Done!')
