'''

namedtuple methods returns a tuple-like object that has fields accessible with named indexes as well as
integer indexes of normal tuples.

- useful in an application when there is a large number of tuples and we need to easily keep track of what each tuple represents
'''

from collections import namedtuple
space = namedtuple('space', 'x y z')
sl = space(x = 2.0, y = 4.0, z = 10) #We can also use space(2.0, 4.0 10)
print(sl.x * sl.y *sl.z) #calculates the volume
#Output: 80.0

space2 = namedtuple('space2', 'x y z')
sl = space(x = 2.0, y = 4.0, z = 10) #We can also use space(2.0, 4.0 10)
sl._l

'''
In addition to the inerited tuple methods, the named tuple also defines three methods of its own
_make(), asdict() and _replace. These methods begins with an underscore to prevent potential conflicts with field names.

The _make() method takes an iterable as an rgument and turns it into a names tuple object.

For example,
'''

sl = [4, 5, 6]
space._make(sl)
space(x = 4, y = 5, z = 6)

'''
The _asdict method and returns an OrderedDict with the field names mapped to index keys and the values mapped
to the dictionary values. For example,
'''

sl._asdict()
#output: OrderedDict([('x', 3), ('_l', 4), ('z', 5)])
