a=10
b=20

def my_function():
    global a
    a=11
    b=21

my_function()
print(a)
print(b)

x='one'

if x==0:
    print('False')
elif x==1:
    print('True')
else:
    print('Something Else')


x=0
while x < 3:
    print(x);
    x+=1


#if a==b: #a and b have the same value
#if a is b: #if a and b are the same object
#if type(a) is type(b): #a and b have same type

#Lists are mutable.

#Strings are immutable

'''

##### About Strings #####

1] s.count(substring, [start, end])
Counts the occurrences of a substring with optional start and end parameters.

2] s.expandtabs([tabsize])
Replaces tabs with spaces.

3] s.find(substring, [start, end])
Returns the index of the first occurrence of a substring or returns -1 if the substring is not found.

4] s.isalnum()
Returns True if all characters are alphanumeric, returns False otherwise.

5] s.isalpha()
Returns True if all characters are alphabetic, returns False otherwise.

6] s.isdigit()
Returns True if all characters are digits, returns False otherwise.

7] s.join(t)
Joins the strings in sequence t.

8] s.lower()
Converts the string to all lowercase.

9] s.replace(old, new [maxreplace])
Replaces old substring with new substring

10] s.strip([characters])
Removes whitespace or optional characters

11] s.split([separator], [maxsplit])
Splits a string separated by whitespace or an optional separator. Returns a list.


greet = 'hello world'

print(greet[1])
print(greet[0:8])
print(greet[0:8:2])
print(greet[0::2])
print(greet[1+2])
print(greet[len(greet)-1])


for i in enumerate(greet[0:5]):
    print(i)


greet[:5] + ' wonderful' + greet[5:]
Output : 'hello wonderful world'


x='3'; y='2'

x+y # will dp the concatenation
Output: '32'

int(x) + int(y) #addition
Output: 5



##### About Lists #####

1] list(s)
Returns a list of the sequence s

2] s.append(x)
Appends element x to the end of s

3] s.extend(x)
Appends the list x to s

4] s.count(x)
Counts the occurrence of x in s.

5] s.index(x, [start], [stop])
Returns the smallest index, i, where s[i]==x. Can include optional start and stop index for the search.

6] s.insert(i,e)
Inserts e at index i

7] s.pop(i)
Returns the element i and removes it from the list.

8] s.remove(x)
Removes x from s.

9] s.reverse()
Reverse the order of s

10] s.sort(key, [reverse])
Sorts s with optional key and reverse.


* Understanding memory allocation for lists:

list2=list1

Here, list1 and list2 both point to the same slot in memory.


An important feature of list's is that they can contain nested structures -
e.g:

items = [ ["Rice", 2.4, 8], ["Flour", 1.9, 5], ["Corn", 4.7, 6] ]
for item in items:
    print("Product: %s Price: %.2f Quality: %i" % (item[0], item[1], item[2]))

Output:
Product: Rice Price: 2.40 Quality: 8
Product: Flour Price: 1.90 Quality: 5
Product: Corn Price: 4.70 Quality: 6

items[1][1] = items[1][1] * 1.2
print(items[1][1]) #2.28


l = [2, 4, 6, 8, 16]
[i**3 for i in l]
Output: [8, 64, 512, 4096]


def f1(x):
    return x*2

def f2(x):
    return x*4

lst = []
for i in range(16):
    lst.append(f1(f2(i)))

print(lst)
print([f1(x) for x in range(64) if x in [f2(j) for j in range(16)]])

--------------

list1 = [ [1,2,3], [4,5,6] ]
[i*j for i in list1[0] for j in list1[1]]
Output: [4, 5, 6, 8, 10, 12, 12, 15, 18]

'''


words = 'here is a sentence'.split()
print([[word, len(word)] for word in words])



#Higher order functions in Python3: filter and map
lst = [1,2,3,4]
print(list(map(lambda x: x**3, lst)))

print(list(filter(lambda x: x<3, lst)))

words = str.split('The longest word in this sentence')
print(words)
print(sorted(words, key=len))

str = ['A','b','a','C','c']
print(str)
